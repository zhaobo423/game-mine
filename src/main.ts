import { createApp } from 'vue'
import "./assets/font/iconfont.css";
import App from './App.vue'

createApp(App).mount('#app')
